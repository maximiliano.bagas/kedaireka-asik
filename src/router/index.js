import { createWebHistory, createRouter } from "vue-router";
import Beranda from "../components/MenuBeranda.vue";
import Maps from "../components/MenuMaps.vue";
import Bantuan from "../components/MenuBantuan.vue";
import Login from "../components/MenuLogin.vue";
import Admin_Page from "../components/AdminPage.vue";
import Dashboard from "../components/MenuDashboard.vue";
import NotFoundComponent from "../components/NotFoundComponent.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "Dashboard",
      component: Dashboard,
    },
    {
      path: "/Beranda",
      name: "Beranda",
      component: Beranda,
    },
    {
      path: "/Maps",
      name: "Maps",
      component: Maps,
    },
    {
      path: "/Bantuan",
      name: "Bantuan",
      component: Bantuan,
    },
  
    {
      path: "/Login",
      name: "Login",
      component: Login,
    },
    {
      path: "/Admin_Page",
      name: "Admin_Page",
      component: Admin_Page,
    },
    { path: '/:pathMatch(.*)',
      component: NotFoundComponent 
    }
  ],
});

// const isAuthenticated = false;

router.beforeEach((to, _from, next)=>{
  const isAuthenticated = JSON.parse(localStorage.getItem('authenticated'));
  setTimeout(() => {
    localStorage.removeItem('authenticated');
  }, 3600000);
  if(to.name == "Admin_Page" && !isAuthenticated) next({name: "Login"});
  else next();
})

export default router;
